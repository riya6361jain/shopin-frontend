import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyAQhs5hXZfvbDrTIjJRMmMBa1_Vi5SaK6M",
  authDomain: "shop-in-7b8d8.firebaseapp.com",
  projectId: "shop-in-7b8d8",
  storageBucket: "shop-in-7b8d8.appspot.com",
  messagingSenderId: "1035329743660",
  appId: "1:1035329743660:web:19fbe978b09341ce139a89",
  measurementId: "G-C2XV6VFYY8"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };