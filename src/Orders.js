import React, { useState, useEffect } from 'react';
import './Orders.css';
import axios from "./axios";
import { useStateValue } from "./StateProvider";
import SingleOrder from './SingleOrder';

function Orders() {
    const [{ user }, dispatch] = useStateValue();
    const [orders, setOrders] = useState([]);


    useEffect(() => {
        if (user) {

            axios.get('http://localhost:3001/read').then((response) => {
                // console.log(response.data);
                setOrders(response.data);
            })
        } else {
           setOrders([]);  
        }

    }, [user,orders])

    return (
        <div className='orders'>
            

            <h1>Your Orders</h1>
            <p>{user? ' ': 'Please Sign-in'}</p>
            <div className={'orders__order' && user}>
                {orders.map(item => (
                    <SingleOrder
                        id={item._id}
                        order = {item.basket}
                        amount={item.amount}
                        createdAt={item.createdAt}
                    />
                ))}
            </div>
           
        </div>
    )
}

export default Orders