import React from "react";
import "./Home.css";
import Product from "./Product";


import HeroSlider, {
  Slide,
  Nav,
  SideNav,
  MenuNav,
  ButtonsNav,
  AutoplayButton,
  OverlayContent,
  OverlayContainer
} from 'hero-slider';

function Home() {
  return (
    <div className="home">
      <div className="home_container">
        <HeroSlider
          slidingAnimation="left_to_right"
          orientation="horizontal"
          initialSlide={1}
          style={{
            // color: '#FFF'
          }}
          settings={{
            slidingDuration: 500,
            slidingDelay: 200,
            shouldAutoplay: true,
            shouldDisplayButtons: true,
            autoplayDuration: 5000,
            height: '75vmin',
          }}
        >
          <Slide
            background={{
              backgroundImage: 'https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/May/Hero/Fuji_TallHero_45M_v2_1x._CB432458380_.jpg',
              // backgroundAttachment: 'fixed',
              backgroundPosition: 'center center',
            }}
          />
          <Slide
            background={{
              backgroundImage: 'https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/May/Hero/Fuji_TallHero_Computers_1x._CB432469755_.jpg',
              // backgroundAttachment: 'fixed',
              backgroundPosition: 'center center',
            }}
          />
          <Slide
            background={{
              backgroundImage: "https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/May/Hero/Fuji_TallHero_Toys_en_US_1x._CB431858161_.jpg",
              // backgroundAttachment: 'fixed',
              backgroundPosition: 'center center',
            }}
          />
          <Slide
            background={{
              backgroundImage: "https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/May/Hero/Fuji_TallHero_Beauty_v2_en_US_1x._CB429089975_.jpg",
              // backgroundAttachment: 'fixed',
              backgroundPosition: 'center center',
            }}
          />
          <Nav />
        </HeroSlider>

        <div className="home_row">
          <Product
            id="4903850"
            title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor"
            price={14999}
            rating={3}
            image="https://images-na.ssl-images-amazon.com/images/I/71Swqqe7XAL._AC_SX466_.jpg"
          />
          <Product
            id="23445930"
            title="Amazon Echo (3rd generation) | Smart speaker with Alexa, Charcoal Fabric"
            price={7425}
            rating={5}
            image="https://media.very.co.uk/i/very/P6LTG_SQ1_0000000071_CHARCOAL_SLf?$300x400_retinamobilex2$"
          />
          <Product
            id="3254354345"
            title="New Apple iPad Pro (12.9-inch, Wi-Fi, 128GB) - Silver (4th Generation)"
            price={44925}
            rating={4}
            image="https://images-na.ssl-images-amazon.com/images/I/816ctt5WV5L._AC_SX385_.jpg"
          />
        </div>

        <div className="home_row">
          <Product
            id="90829332"
            title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor - Super Ultra Wide Dual WQHD 5120 x 1440"
            price={82123}
            rating={4}
            image="https://images-na.ssl-images-amazon.com/images/I/6125mFrzr6L._AC_SX355_.jpg"
          />
        </div>
      </div>
    </div>
  );
}

export default Home;
