import "./App.css";
import Header from "./Header";
import Home from "./Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Checkout from "./Checkout";
import Login from "./Login";
import { auth } from "./firebase";
import { useStateValue } from "./StateProvider";
import { useEffect } from "react";
import Payment from "./Payment";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import Orders from "./Orders";

const promise = loadStripe(
  "pk_test_51IXSnkSASMmskBpRG6RDpXqiqk5ev6aBkbhTKnyI5ALHTSEJGCDIXz1eiRzaYk5CrG9GjuuHs74stSDH8vIBkL1000kQ0WADrL"
);

function App() {
  const [{}, dispatch] = useStateValue();
  useEffect(() => {
    //will run only once
    auth.onAuthStateChanged((authUser) => {
      console.log(authUser);

      if (authUser) {
        //loggedin
        dispatch({
          type: "SET_USER",
          user: authUser,
        });
      } else {
        //loggedout
        dispatch({
          type: "SET_USER",
          user: null,
        });
      }
    });
  }, []);

  return (
    <Router>
      <div className="app">
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/checkout">
            <Header />
            <Checkout />
          </Route>
          <Route path="/orders">
            <Header />
            <Orders />
          </Route>
          <Route path="/payment">
            <Header />
            <Elements stripe={promise}>
              <Payment />
            </Elements>
          </Route>
          <Route path="/">
            <Header />
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
