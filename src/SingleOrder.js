import React from 'react'
import './SingleOrder.css'
import moment from "moment";
import CheckoutProduct from "./CheckoutProduct";
import CurrencyFormat from "react-currency-format";

function SingleOrder({ id, order, amount, createdAt }) {



    return (
        <div className='singleorder'>
            <h2>Order</h2>
            <p>{moment.unix(createdAt).format("MMMM Do YYYY, h:mma")}</p>
            <p className="singleorder__id">
                <small>{id}</small>
            </p>

            {order?.map(item => (
                <CheckoutProduct
                    id={item.id}
                    title={item.title}
                    image={item.image}
                    price={item.price}
                    rating={item.rating}
                    hideButton
                />
            ))}
            <CurrencyFormat
                renderText={(value) => (
                    <h3 className="singleorder__total">Total: {value}</h3>
                )}
                decimalScale={2}
                value={amount / 100}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"₹"}
            />
        </div>
    )
}

export default SingleOrder