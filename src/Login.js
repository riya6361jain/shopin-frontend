import React, { useState } from "react";
import { Link,useHistory } from "react-router-dom";
import { auth } from "./firebase";
import "./Login.css";

function Login() {
  const history = useHistory();
const [email,setEmail] = useState('');
const [password, setpassword] = useState('');

const signIn = e => {
     e.preventDefault();

     auth
         .signInWithEmailAndPassword(email, password)
         .then(auth => {
           history.push('/')
         })
         .catch(error => alert(error.message))
}

const register = e => {
    e.preventDefault();

    auth
    .createUserWithEmailAndPassword(email,password)
    .then((auth) => {
      console.log(auth);
      if(auth) {
        history.push('/')
      }
    })
    .catch(error => alert(error.message))
}
  return (
    <div className="login">
      <Link to="/">
        <img
          className="login_logo"
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTU-6k0T_XnR4xJLXKTLvWn4r1xVSGSR3pAoQ&usqp=CAU"
          alt=""
        />
      </Link>

      <div className="login_form">
        <h2>Sign-in</h2>

        <form>
            <h5>E-mail</h5>
          <input type="email" value={email} onChange={e => setEmail(e.target.value)}/>
           <h5>Password</h5>
          <input type="password" value={password} onChange={e => setpassword(e.target.value)} />
          <button type='submit' onClick={signIn} className='login_signInBtn'>Sign In</button>
        </form> 
        <button onClick={register} className='login_registerBtn'>Create your Account</button>

      </div>
    </div>
  );
}

export default Login;
